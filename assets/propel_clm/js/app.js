
document.onreadystatechange = function() {
    if (document.readyState !== "complete") {
        document.querySelector("body").style.visibility = "hidden";
        document.querySelector("#loader").style.visibility = "visible";
    } else {
        document.querySelector("#loader").style.display = "none";
        document.querySelector("body").style.visibility = "visible";
    }
  };
  
  window.addEventListener('scroll', function() {
    if (window.scrollY > 50) {
      document.getElementById('navbarNav').classList.remove('mt-4');
      document.getElementById('mainNav').classList.add('fixed-top');
      document.getElementById('mainNav').classList.add('nav-custom-bg');
      // add padding top to show content behind navbar
      navbar_height = document.querySelector('#main-nav').offsetHeight;
      document.body.style.paddingTop = navbar_height + 'px';
    } else {
      document.getElementById('navbarNav').classList.add('mt-4');
      document.getElementById('mainNav').classList.remove('fixed-top');
      document.getElementById('mainNav').classList.remove('nav-custom-bg');
       // remove padding top from body
      document.body.style.paddingTop = '0';
    } 
  });


  // Get the button:
let mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
  
  
  
  function aboutFun(){
    var scrollDiv = document.getElementById("AboutLE").offsetTop;
  window.scrollTo({ top: scrollDiv-80, behavior: 'smooth'});
  }
  
  function panelistsFun(){
    var scrollDiv = document.getElementById("Presenters").offsetTop;
  window.scrollTo({ top: scrollDiv-100, behavior: 'smooth'});
  }
  
  function eventFun(){
    var scrollDiv = document.getElementById("Event").offsetTop;
  window.scrollTo({ top: scrollDiv-100, behavior: 'smooth'});
  }
  
  function storyFun(){
    var scrollDiv = document.getElementById("Success").offsetTop;
  window.scrollTo({ top: scrollDiv-100, behavior: 'smooth'});
  }
  
  function regFun(){
    var scrollDiv = document.getElementById("hero").offsetTop;
  window.scrollTo({ top: scrollDiv-100, behavior: 'smooth'});
  }